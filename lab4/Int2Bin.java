public class Int2Bin {
public static void main(String []args) {

    
    int decNum = Integer.parseInt(args[0]);

    int remainder = 0;
    for (int i = 1; decNum > 0; i++) {

            decNum /= 2;
            remainder = decNum % 2;
            System.out.print(remainder);
        }

    }
}