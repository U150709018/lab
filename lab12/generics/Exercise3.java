package generics;
public class Exercise3 {
    public static void main(String[] args) {
    	System.out.printf("Maximum of %d, %d and %d is %d\n\n", 3, 4, 5, max(3, 4, 5));
    	System.out.printf("Maximum of %.2f, %.2f and %.2f is %.2f\n\n", 6.6, 8.8, 7.7, max(6.67, 8.89,7.75));
    	System.out.printf("Maximum of %s, %s and %s is %s\n", "pear", "apple", "orange",max("pear", "apple", "orange"));
    }
	
    static <T extends Comparable<T>> T max(T x, T y, T z) {
    	
    	T max = x;
    	if(y.compareTo(max) > 0) {
    		max=y;
    	}
    	if(z.compareTo(max) > 0) {
    		max=z;    		
    	}
    	
    	return max;
        
        }
	}
