package enes.shapes3d;
import enes.shapes.Square;

public class Cube extends Square {
    public Cube(){
        this(5);
    }
    public Cube(int s){
       super(s);
        System.out.println("Creating a cube with side = " + s);
    }

    @Override
    public int area(){
        return super.area() * 6;
    }

    public int volume(){
        return super.area() * side;
    }
	
    @Override
    public String toString(){
        return "Side: " + side + ", Area: " + area() + ", Volume: " + volume();
    }
}