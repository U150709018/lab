package enes.main;
import java.util.ArrayList;
import enes.shapes3d.*;
public class Test3D {
	public static void main(String[] args) {
        cylinderTest();
        cubeTest();
	}
 
	public static void cylinderTest(){
        ArrayList<Cylinder> cylinders = new ArrayList<>();
        Cylinder cylinder = new Cylinder();

        cylinders.add(cylinder);
        cylinders.add(new Cylinder(6,7));
        
		System.out.println("\nAttributes of cylinders\n" + cylinders + "\n");
    }
    public static void cubeTest(){
        
		ArrayList<Cube> cubes = new ArrayList<>();
        Cube cube = new Cube();

        cubes.add(cube);
        cubes.add(new Cube(9));
        System.out.println("\nAttributes of cubes\n" + cubes);
		//
    }
}