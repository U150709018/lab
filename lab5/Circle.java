public class Circle {
	int radius;
	
	public double area() {
		double area = (radius * radius) * 3.14;
		return area;
	}
	
	public double perimeter() {
		double perimeter = 2 * 3.14 * radius;
		return perimeter;
	}
}