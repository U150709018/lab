public class Rectangle {
	int sideA;
	int sideB;
	public int area() {
		int area = sideA *sideB;
		return area;
	}
	
	public int perimeter() {
		int perimeter = 2 * (sideA + sideB);
		return perimeter;
	}

}